import 'package:aplikasi_ortu/presentation/app_bindings.dart';
import 'package:get/get.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/login/bindings/login_binding.dart';
import '../modules/login/views/login_view.dart';
import '../modules/nilaiujian/bindings/nilaiujian_binding.dart';
import '../modules/nilaiujian/views/nilaiujian_view.dart';
import '../modules/prelogin/bindings/prelogin_binding.dart';
import '../modules/prelogin/views/prelogin_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.LOGIN;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: AppBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: AppBinding(),
    ),
    GetPage(
      name: _Paths.PRELOGIN,
      page: () => const PreloginView(),
      binding: PreloginBinding(),
    ),
    GetPage(
      name: _Paths.NILAIUJIAN,
      page: () => NilaiujianView(),
      binding: NilaiujianBinding(),
    ),
  ];
}
