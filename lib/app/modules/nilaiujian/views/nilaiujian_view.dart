import 'package:aplikasi_ortu/app/config/app_colors.dart';
import 'package:aplikasi_ortu/app/modules/login/controllers/login_controller.dart';
import 'package:aplikasi_ortu/app/modules/nilaiujian/views/kuis_view.dart';
import 'package:aplikasi_ortu/app/modules/nilaiujian/views/uas_view.dart';
import 'package:aplikasi_ortu/app/modules/nilaiujian/views/ulangan_view.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../../home/controllers/home_controller.dart';
import '../controllers/nilaiujian_controller.dart';
import 'uts_view.dart';

class NilaiujianView extends GetView<NilaiujianController> {
  final HomeController homeController = Get.find<HomeController>();
  final LoginController loginController = Get.find<LoginController>();

  NilaiujianView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(child: Text('Nilai ujian')),
        leading: Container(
          padding: EdgeInsets.only(left: 10.0),
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.back(result: {
                'username': Get.arguments['username'],
                'class': Get.arguments['class'],
                'year': Get.arguments['year'],
              });
            },
          ),
        ),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(20.0),
          ),
        ),
        backgroundColor: AppColors.edbxPrimary,
        toolbarHeight: 80,
      ),
      body: _bodyNilaiUjian(),
    );
  }

  Widget _bodyNilaiUjian() {
    List<String> textList = [
      'Ujian tengah semester',
      'Ujian akhir semester',
      'Kuis',
      'Ulangan'
    ];
    return Padding(
      padding: EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 0),
      child: GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            crossAxisSpacing: 30.0,
            mainAxisSpacing: 40.0,
          ),
          itemCount: 4,
          itemBuilder: (context, index) {
            return GestureDetector(
              onTap: () {
                if (index == 0) {
                  //UTS
                  Get.to(() => uts_view());
                  // Get.to(uts_view());
                } else if (index == 1) {
                  //UAS
                  Get.to(() => uas_view());
                  // Get.to(uas_view());
                } else if (index == 2) {
                  //Kuis
                  Get.to(() => kuis_view());
                  // Get.to(kuis_view());
                } else if (index == 3) {
                  //Ulangan
                  Get.to(() => ulangan_view());
                  // Get.to(ulangan_view());
                }
              },
              child: Column(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color.fromARGB(255, 147, 184, 135),
                          borderRadius: BorderRadius.circular(15.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.shade600,
                                offset: Offset(0, 4),
                                blurRadius: 10)
                          ]),
                      child: Container(
                        padding: EdgeInsets.fromLTRB(0, 100.0, 0, 0),
                        child: Center(
                          child: Text(
                            textList[index],
                            style: TextStyle(
                                color: AppColors.edbxPrimary,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 8.0),
                ],
              ),
            );
          }),
    );
  }
}
