import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/nilaiujian_controller.dart';

class uts_view extends GetView<NilaiujianController> {
  @override
  Widget build(BuildContext context) {
    final NilaiujianController _controller = Get.put(NilaiujianController());
    return Scaffold(
      appBar: AppBar(
        title: Container(child: Text('Ujian tengah semester')),
        leading: Container(
          padding: EdgeInsets.only(left: 10.0),
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(20.0),
        )),
        backgroundColor: Color.fromARGB(255, 2, 65, 2),
        toolbarHeight: 80,
      ),
      body: Container(
        padding: EdgeInsets.only(top: 20.0),
        child: ListView.builder(
          itemCount: 1, // Display a single item
          itemBuilder: (context, index) {
            return Center(
              child: Obx(() {
                if (_controller.score.value == 0.0) {
                  return CircularProgressIndicator();
                } else {
                  return Text(
                    'Score: ${_controller.score.value.toStringAsFixed(2)}',
                    style: TextStyle(fontSize: 16),
                  );
                }
              }),
            );
          },
        ),
      ),
    );
  }
}
