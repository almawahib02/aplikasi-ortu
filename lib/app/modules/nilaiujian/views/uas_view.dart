import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/nilaiujian_controller.dart';

class uas_view extends GetView<NilaiujianController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(child: Text('Ujian akhir semester')),
        leading: Container(
          padding: EdgeInsets.only(left: 10.0),
          child: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Get.back();
            },
          ),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(20.0),
        )),
        backgroundColor: Color.fromARGB(255, 2, 65, 2),
        toolbarHeight: 80,
      ),
      body: Center(
        child: Text('This is the second screen'),
      ),
    );
  }
}
