import 'package:aplikasi_ortu/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/prelogin_controller.dart';

class PreloginView extends GetView<PreloginController> {
  const PreloginView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 2, 65, 2),
      body: _loginManual(context),
    );
  }

  Widget _loginManual(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return SingleChildScrollView(
        child: Form(
            child: Column(
          children: [
            SizedBox(height: 100.0),
            Center(
              child: Container(
                width: constrains.maxWidth,
                height: constrains.maxHeight * 0.2,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                child: Stack(
                  children: [
                    Center(
                      child: Text(
                        'KP',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: constrains.maxWidth * 0.15,
                vertical: constrains.maxHeight * 0.06,
              ),
              child: Column(
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Assalamualaikum',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Silahkan login atau register',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: const Text(
                      'Untuk tahu tentang siswa',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(height: 50.0),
                  Container(
                    alignment: Alignment.center,
                    child: SizedBox(
                      height: constrains.maxHeight * 0.05,
                      width: constrains.maxWidth * 0.1,
                      child: ElevatedButton(
                        onPressed: () {},
                        child: Text(
                          '-',
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color?>(
                                  (Set<MaterialState> states) {
                            if (states.contains(MaterialState.pressed))
                              return const Color.fromARGB(255, 137, 180, 137);
                            return Colors.white;
                          }),
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                              side: BorderSide(
                                width: 1,
                                color: Colors.black,
                              ), // Set the border width and color
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      '-ATAU-',
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: constrains.maxWidth * 0.1,
                        vertical: constrains.maxHeight * 0),
                    child: Column(
                      children: [
                        TextFormField(
                          // maxLength: 15,
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            hintText: "Email atau no. handphone",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            hintStyle: TextStyle(
                              color: Color.fromARGB(255, 128, 128, 128),
                            ),
                          ),
                        ),
                        // SizedBox(height: 0.5),
                        TextFormField(
                          style: TextStyle(color: Colors.white),
                          decoration: InputDecoration(
                            hintText: "Password",
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            hintStyle: TextStyle(
                              color: Color.fromARGB(255, 128, 128, 128),
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          // padding: EdgeInsets.fromLTRB(2, 0, 0, 0),
                          child: TextButton(
                              onPressed: () {},
                              child: Text(
                                'lupa password',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              )),
                        ),
                        SizedBox(height: 15.0),
                        Container(
                          alignment: Alignment.center,
                          child: SizedBox(
                            height: constrains.maxHeight * 0.05,
                            width: constrains.maxWidth * 0.4,
                            child: ElevatedButton(
                              onPressed: () {},
                              child: Text(
                                'Login',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontStyle: FontStyle.normal,
                                ),
                              ),
                              style: ButtonStyle(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color?>(
                                        (Set<MaterialState> states) {
                                  if (states.contains(MaterialState.pressed))
                                    return const Color.fromARGB(
                                        255, 137, 180, 137);
                                  return Colors.white;
                                }),
                                shape:
                                    MaterialStateProperty.all<OutlinedBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20),
                                    side: BorderSide(
                                      width: 1,
                                      color: Colors.black,
                                    ), // Set the border width and color
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Belum punya akun ?',
                          style: TextStyle(fontSize: 13, color: Colors.white),
                        ),
                        SizedBox(
                          width: 0,
                        ),
                        TextButton(
                            onPressed: () {
                              Get.offNamed(Routes.LOGIN);
                            },
                            child: Text(
                              textAlign: TextAlign.left,
                              'Register',
                              style: TextStyle(
                                  color: Colors.green,
                                  fontSize: 13,
                                  fontWeight: FontWeight.w600),
                            )),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        )),
      );
    });
  }
}
