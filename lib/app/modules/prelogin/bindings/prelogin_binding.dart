import 'package:get/get.dart';

import '../controllers/prelogin_controller.dart';

class PreloginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PreloginController>(
      () => PreloginController(),
    );
  }
}
