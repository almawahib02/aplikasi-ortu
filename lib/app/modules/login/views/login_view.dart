import 'package:aplikasi_ortu/app/config/app_colors.dart';
import 'package:aplikasi_ortu/app/config/app_constants.dart';
import 'package:aplikasi_ortu/app/config/app_text_styles.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/login_controller.dart';

import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';

// ignore: must_be_immutable
class LoginView extends GetView<LoginController> {
  LoginView({Key? key}) : super(key: key);

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.edbxPrimary,
      body: _buildOtpBody(context),
    );
  }

  Widget _buildOtpBody(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                const SizedBox(height: 50.0),
                Center(
                  child: Container(
                    width: constraints.maxWidth,
                    height: constraints.maxHeight * 0.2,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.edbxWhite,
                    ),
                    child: Stack(
                      children: [
                        Center(
                          child: Text(
                            Constants.loginLogo,
                            style: AppTextStyle.loginLogo,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: constraints.maxWidth * 0.1,
                    vertical: constraints.maxHeight * 0.05,
                  ),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          Constants.login,
                          textAlign: TextAlign.left,
                          style: AppTextStyle.login,
                        ),
                      ),
                      TextFormField(
                        keyboardType: TextInputType.number,
                        maxLength: 12,
                        style: TextStyle(color: AppColors.edbxWhite),
                        decoration: InputDecoration(
                          hintText: Constants.phoneHintText,
                          labelText: Constants.phoneLabelText,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.edbxWhite),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: AppColors.edbxWhite),
                          ),
                          prefix: controller.showPrefix.value
                              ? const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8),
                                  child: Text(
                                    Constants.prefixPhoneText,
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold),
                                  ),
                                )
                              : null,
                          suffixIcon: AnimatedOpacity(
                            opacity: controller.phoneNo.value.length == 12
                                ? 1.0
                                : 0.0,
                            duration: const Duration(milliseconds: 250),
                            child: Icon(Icons.check_circle,
                                color: AppColors.edbxGreen, size: 15),
                          ),
                          hintStyle: TextStyle(
                            color: AppColors.edbxGrey1,
                          ),
                        ),
                        onChanged: (val) {
                          controller.phoneNo.value = val;
                          controller.showPrefix.value = val.isNotEmpty;
                        },
                        onSaved: (val) => controller.phoneNo.value = val!,
                        validator: (val) => (val!.isEmpty || val.length < 10)
                            ? Constants.validatorPhoneTextFalse
                            : null,
                      ),
                      // SizedBox(height: 20.0),
                      Row(
                        children: [
                          Text(
                            Constants.resendingText,
                            style: TextStyle(
                              fontSize: 14,
                              color: AppColors.edbxWhite,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            child: TextButton(
                              onPressed: () => controller.resendOTP.value
                                  ? controller.resendOTP()
                                  : null,
                              style: TextButton.styleFrom(
                                alignment: Alignment.centerLeft,
                              ),
                              child: Text(
                                controller.resendOTP.value
                                    ? Constants.resendingTextBtn
                                    : "- ${controller.resendAfter}",
                                style: TextStyle(
                                    fontSize: 14,
                                    color: AppColors.edbxWhite,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),

                      const SizedBox(height: 5.0),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: constraints.maxHeight * 0.05,
                      width: constraints.maxWidth * 0.8,
                      child: ElevatedButton(
                        onPressed: () {
                          final form = _formKey.currentState;
                          if (form!.validate()) {
                            form.save();
                            controller.getOtp();
                          }
                          // _controller.registerUser;
                        },
                        style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color?>(
                                  (Set<MaterialState> states) {
                            if (states.contains(MaterialState.pressed)) {
                              return AppColors.edbxGreenLight;
                            }
                            return AppColors.edbxWhite;
                          }),
                          shape: MaterialStateProperty.all<OutlinedBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                              side: BorderSide(
                                width: 1,
                                color: AppColors.edbxBlack,
                              ), // Set the border width and color
                            ),
                          ),
                        ),
                        child: Text(
                          Constants.sendingText,
                          style: TextStyle(
                            color: AppColors.edbxBlack,
                            fontSize: 16,
                            fontStyle: FontStyle.normal,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 50.0),
                Center(
                  child: Text(
                    Constants.otpText,
                    style: TextStyle(
                      color: AppColors.edbxWhite,
                      fontSize: 16,
                      fontStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Container(
                  constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.8),
                  child: OtpTextField(
                    numberOfFields: 6,
                    borderColor: AppColors.edbxGreenLight,
                    showFieldAsBox: false,
                    textStyle: TextStyle(color: AppColors.edbxWhite),
                    onSubmit: (String verificationCode) {
                      controller.otp.value = verificationCode;
                    },
                  ),
                ),
                const SizedBox(height: 60),
                Container(
                  alignment: Alignment.center,
                  child: SizedBox(
                    height: constraints.maxHeight * 0.05,
                    width: constraints.maxWidth * 0.4,
                    child: ElevatedButton(
                      onPressed: () {
                        controller.verifyOTP();
                      },
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color?>(
                                (Set<MaterialState> states) {
                          if (states.contains(MaterialState.pressed)) {
                            return AppColors.edbxGreenLight;
                          }
                          return AppColors.edbxWhite;
                        }),
                        shape: MaterialStateProperty.all<OutlinedBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                            side: BorderSide(
                              width: 1,
                              color: AppColors.edbxBlack,
                            ), // Set the border width and color
                          ),
                        ),
                      ),
                      child: Text(
                        Constants.login,
                        style: TextStyle(
                          color: AppColors.edbxBlack,
                          fontSize: 16,
                          fontStyle: FontStyle.normal,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
