import 'dart:async';
import 'dart:convert';

import 'package:aplikasi_ortu/app/modules/home/controllers/home_controller.dart';
import 'package:aplikasi_ortu/data/models/user_model.dart' as LocalUser;
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../config/config.dart';
import '../../../routes/app_pages.dart';

class LoginController extends GetxController
    with GetSingleTickerProviderStateMixin {
  var showPrefix = false.obs;
  var isLogin = true;
  var phoneNo = "".obs;
  var otp = "".obs;
  var isOtpSent = false.obs;
  var resendAfter = 30.obs;
  var resendOTP = false.obs;
  var firebaseVerificationId = "";
  var statusMessage = "".obs;
  var statusMessageColor = Colors.black.obs;
  var parentId = ''.obs;

  final FirebaseAuth auth = FirebaseAuth.instance;
  final dio = Dio();

  var timer;

  LoginController();

  @override
  void onInit() {
    super.onInit();
  }

  getOtp() async {
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+62${phoneNo.value}',
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        firebaseVerificationId = verificationId;
        isOtpSent.value = true;
        statusMessage.value = "OTP sent to +62${phoneNo.value}";
        startResendOtpTimer();
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  resendOtp() async {
    resendOTP.value = false;
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+62${phoneNo.value}',
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        firebaseVerificationId = verificationId;
        isOtpSent.value = true;
        statusMessage.value = "OTP re-sent to +62${phoneNo.value}";
        startResendOtpTimer();
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  verifyOTP() async {
    final HomeController hController = HomeController();
    try {
      statusMessage.value = "Verifying... ${otp.value}";
      // Create a PhoneAuthCredential with the code
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
          verificationId: firebaseVerificationId, smsCode: otp.value);
      // Sign the user in (or link) with the credential
      await auth.signInWithCredential(credential);
      await hController.studentData();

      final user = auth.currentUser;
      if (user != null) {
        final phoneNumber =
            (auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
        final email = '${phoneNumber.replaceFirst('62', '')}@gmail.com';

        final checkAccountUrl =
            '${Config.baseUrl}users?username=$phoneNumber';
        debugPrint('URL: ${checkAccountUrl.toString()}');
        
        dio.options.headers['Content-type'] = 'application/json';
        dio.options.headers['Authorization'] = 'Bearer ${Config.bearerToken}';

        final checkAccountResponse = await dio.get(checkAccountUrl);
        if (checkAccountResponse.statusCode == 200) {
          debugPrint(
              'status code: ${checkAccountResponse.statusCode.toString()}');
          debugPrint('ok');
          final accountData = checkAccountResponse.data;

          if (accountData != null && accountData.isNotEmpty) {
            debugPrint('Account already exists! Going to home');
            parentId.value =
                accountData['data'][0]['_id']; // Get the '_id' for parentId
            debugPrint('Parent ID: $parentId');
            Get.offNamed(Routes.HOME, arguments: parentId);
            return;
          }
        } else {
          debugPrint('Account not found, creating a new account');
        }

        final body = LocalUser.User(
          username: phoneNumber,
          email: email,
          password: phoneNumber,
          phone: phoneNumber,
        );

        final jsonBody = jsonEncode(body.toJson());
        debugPrint('Account creation request body: $jsonBody');
        final response =
            await dio.post('${Config.baseUrl}users', data: body.toJson());
        if (response.statusCode == 200 || response.statusCode == 201) {
          debugPrint('Account created successfully');
          debugPrint(response.statusCode.toString());
        } else {
          debugPrint('Account failed to create');
          debugPrint(response.statusCode.toString());
          // Handle the server error gracefully
          statusMessage.value =
              "Account creation failed. Please try again later.";
          statusMessageColor = Colors.red.obs;
          return;
        }
      }
    } catch (e) {
      if (e is DioError && e.response?.statusCode == 500) {
        // Handle the 500 error silently without printing debug logs
        statusMessage.value =
            "Account creation failed. Please try again later.";
        statusMessageColor = Colors.red.obs;
        return;
      }

      // Handle other errors as usual
      statusMessage.value = "Invalid OTP";
      statusMessageColor = Colors.red.obs;
    }
  }

  startResendOtpTimer() {
    resendAfter.value = 30;
    resendOTP.value = false;
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (resendAfter.value > 0) {
        resendAfter.value--;
      } else {
        resendOTP.value = true;
        timer.cancel();
      }
    });
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }
}
