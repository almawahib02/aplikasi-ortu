import 'dart:async';
import 'package:aplikasi_ortu/domain/repositories/login_repository.dart';
import 'package:aplikasi_ortu/domain/repositories/user_repository.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../../app_preferences.dart';

class LoginController extends GetxController
    with GetSingleTickerProviderStateMixin {
  var showPrefix = false.obs;
  var isLogin = true;
  var phoneNo = "".obs;
  var otp = "".obs;
  var isOtpSent = false.obs;
  var resendAfter = 30.obs;
  var resendOTP = false.obs;
  var firebaseVerificationId = "";
  var statusMessage = "".obs;
  var statusMessageColor = Colors.black.obs;

  final UserRepository _userRepo;
  final LoginRepository _loginRepo;

  LoginController(this._userRepo, this._loginRepo);

  final FirebaseAuth auth = FirebaseAuth.instance;

  var timer;

  getOtp() async {
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+62${phoneNo.value}',
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        firebaseVerificationId = verificationId;
        isOtpSent.value = true;
        statusMessage.value = "OTP sent to +62${phoneNo.value}";
        startResendOtpTimer();
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  resendOtp() async {
    resendOTP.value = false;
    FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: '+62${phoneNo.value}',
      verificationCompleted: (PhoneAuthCredential credential) {},
      verificationFailed: (FirebaseAuthException e) {},
      codeSent: (String verificationId, int? resendToken) {
        firebaseVerificationId = verificationId;
        isOtpSent.value = true;
        statusMessage.value = "OTP re-sent to +62${phoneNo.value}";
        startResendOtpTimer();
      },
      codeAutoRetrievalTimeout: (String verificationId) {},
    );
  }

  Future<void> createAccountAPI() async {
    final parentId = await _userRepo.checkAccount();
    if (parentId != null) {
      debugPrint(parentId);
      await _loginRepo.loginUserApi();
      parentIds(parentId);
      studentData();
    } else {
      debugPrint('Parent ID is null. Cannot proceed with login.');
    }
  }

  verifyOTP() async {
    String otpValue = otp.value;
    debugPrint('OTP auth: $otpValue');
    debugPrint(firebaseVerificationId);
    if (isOtpSent.value) {
      PhoneAuthCredential credential = PhoneAuthProvider.credential(
        verificationId: firebaseVerificationId,
        smsCode: otpValue,
      );
      await auth.signInWithCredential(credential).then((value) async {
        await createAccountAPI();
      });
    }
  }

  startResendOtpTimer() {
    timer = Timer.periodic(const Duration(seconds: 1), (timer) {
      if (resendAfter.value != 0) {
        resendAfter.value--;
      } else {
        resendAfter.value = 30;
        resendOTP.value = true;
        timer.cancel();
      }
      update();
    });
  }

  @override
  void onClose() {
    timer?.cancel();
    super.onClose();
  }

  void parentIds(String value) {}

  void studentData() {}
}
