import 'package:aplikasi_ortu/app/config/app_colors.dart';
import 'package:aplikasi_ortu/app/config/app_constants.dart';
import 'package:aplikasi_ortu/app/config/app_text_styles.dart';
import 'package:aplikasi_ortu/app/modules/nilaiujian/views/nilaiujian_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_slideshow/flutter_image_slideshow.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildBottomNav(),
      body: _homeBody(),
    );
  }

  Widget _homeBody() {
    return LayoutBuilder(builder: (context, constraints) {
      return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: constraints.maxWidth,
              height: constraints.maxHeight * 0.2,
              padding: const EdgeInsets.fromLTRB(0, 20.0, 0, 0),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(25.0),
                      bottomRight: Radius.circular(25.0)),
                  color: AppColors.edbxPrimary),
              child: Row(
                children: [
                  Container(
                    width: constraints.maxWidth * 0.3,
                    height: constraints.maxHeight * 0.08,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: AppColors.edbxWhite),
                    child: Center(
                      child: Obx(() {
                        final username =
                            controller.studentName.value;
                        final initials = username.isNotEmpty
                            ? username.substring(0, 2)
                            : '';
                        return Text(
                          initials,
                          style: TextStyle(
                            color: AppColors.edbxBlack,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        );
                      }),
                    ),
                  ),
                  Column(
                    children: [
                      const Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20)),
                      Container(
                        alignment: Alignment.centerLeft,
                        width: constraints.maxWidth * 0.7,
                        height: constraints.maxHeight * 0.03,
                        child: Text(
                          Constants.salam,
                          style: AppTextStyle.appbarText,
                        ),
                      ),
                      SizedBox(
                        width: constraints.maxWidth * 0.7,
                        height: constraints.maxHeight * 0.03,
                        child: Obx(
                          () => Text(
                            controller.studentName.value,
                            style: TextStyle(
                              color: AppColors.edbxWhite,
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 10.0),
                      Container(
                        margin: const EdgeInsets.fromLTRB(0, 0, 30.0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              Constants.kelasText,
                              style: AppTextStyle.appbarText,
                            ),
                            const SizedBox(width: 5.0),
                            Container(
                              width: constraints.maxWidth * 0.12,
                              height: constraints.maxHeight * 0.03,
                              decoration: BoxDecoration(
                                  color:
                                      const Color.fromARGB(255, 157, 219, 138),
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: Obx(() {
                                return DropdownButton<String>(
                                  padding:
                                      const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                                  value: controller.selectedClass.value,
                                  onChanged: (newValue) {
                                    controller.selectedClass.value =
                                        newValue!;
                                  },
                                  isExpanded: true,
                                  style: AppTextStyle.appbarDrop,
                                  items: controller.classes
                                      .map<DropdownMenuItem<String>>(
                                        (String value) =>
                                            DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      )
                                      .toList(),
                                );
                              }),
                            ),
                            const SizedBox(width: 35.0),
                            Text(
                              Constants.periodText,
                              style: AppTextStyle.appbarText,
                            ),
                            const SizedBox(width: 10.0),
                            Container(
                              width: constraints.maxWidth * 0.17,
                              height: constraints.maxHeight * 0.03,
                              decoration: BoxDecoration(
                                  color: const Color(0xFF9DDB8A),
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: Obx(() {
                                return DropdownButton<String>(
                                  padding:
                                      const EdgeInsets.fromLTRB(8.0, 0, 0, 0),
                                  value: controller.selectedYear.value,
                                  onChanged: (newValue) {
                                    controller.selectedYear.value =
                                        newValue!;
                                  },
                                  isExpanded: true,
                                  style: AppTextStyle.appbarDrop,
                                  items: controller.years
                                      .map<DropdownMenuItem<String>>(
                                        (String value) =>
                                            DropdownMenuItem<String>(
                                          value: value,
                                          child: Text(
                                            value,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      )
                                      .toList(),
                                );
                              }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            ImageSlideshow(
                width: double.infinity,
                height: 200,
                initialPage: 0,
                indicatorColor: AppColors.edbxPrimary,
                indicatorBackgroundColor: AppColors.edbxGrey1,
                children: [
                  Image.asset(
                    'assets/images/class.jpg',
                    fit: BoxFit.cover,
                  ),
                  Image.asset(
                    'assets/images/hall.jpg',
                    fit: BoxFit.cover,
                  ),
                ]),
            Container(
              padding: const EdgeInsets.fromLTRB(25.0, 0, 25.0, 0),
              child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 30.0,
                  mainAxisSpacing: 30.0,
                ),
                itemCount: 9,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      if (index == 0) {
                        //page biaya sekolah
                      } else if (index == 1) {
                        //page kehadiran
                      } else if (index == 2) {
                        //page progress
                      } else if (index == 3) {
                        //page nilai ujian
                        Get.to(() => NilaiujianView(), arguments: {
                          'username': controller.studentName.value,
                          'class': controller.selectedClass.value,
                          'year': controller.selectedYear.value,
                        });
                      } else if (index == 4) {
                        //page eRapor
                      } else if (index == 5) {
                        //page tugas
                      } else if (index == 6) {
                        //page jadwal pelajaran
                      } else if (index == 7) {
                        //page lembar komunikasi
                      } else if (index == 8) {
                        //page pengumuman
                      }
                    },
                    child: Column(
                      children: [
                        Expanded(
                          child: Container(
                            decoration: BoxDecoration(
                                color: const Color.fromARGB(255, 147, 184, 135),
                                borderRadius: BorderRadius.circular(15.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade600,
                                      offset: const Offset(0, 4),
                                      blurRadius: 10)
                                ]),
                            child: Center(
                              child: Icon(
                                Constants.iconMenu[index],
                                color: AppColors.edbxBlack,
                                size: 40,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 10.0),
                        Text(
                          Constants.itemMenuDesc[index],
                          style: AppTextStyle.itemMenuDesc,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _buildBottomNav() {
    return Obx(() => BottomNavigationBar(
          currentIndex: controller.tabIndex.value,
          onTap: (index) {
            // Handle navigation to the desired page based on the index
            if (index == 0) {
              // Navigate to Page 1
              // Get.to(() => Page1());
            } else if (index == 1) {
              // Navigate to Page 2
              // Get.to(() => Page2());
            } else if (index == 2) {
              // Navigate to Page 3
              // Get.to(() => Page3());
            }
            // Add more conditions for other pages as needed

            controller.tabIndex.value = index; // Update the current index
          },
          items: const [
            BottomNavigationBarItem(
              icon:
                  Icon(Icons.home), // Replace with your desired icon for Page 1
              label: 'Page 1',
            ),
            BottomNavigationBarItem(
              icon:
                  Icon(Icons.book), // Replace with your desired icon for Page 2
              label: 'Page 2',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                  Icons.person_2), // Replace with your desired icon for Page 3
              label: 'Page 3',
            ),
          ],
        ));
  }
}
