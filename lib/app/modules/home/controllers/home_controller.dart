import 'package:aplikasi_ortu/domain/repositories/student_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../app_preferences.dart';
import '../../../../data/models/study_group_model.dart';
import '../../../../domain/entities/student.dart';
import '../../login/controllers/login_controller.dart';

class HomeController extends GetxController {
  var selectedClass = ''.obs;
  var selectedYear = ''.obs;
  var studentName = ''.obs;
  var parentId = ''.obs;
  var studentId = ''.obs;
  List<String> classes = [];
  List<String> years = [];
  var tabIndex = 0.obs;

  final StudentRepository _repoStudent;

  HomeController(this._repoStudent);

  get isLoading => null;

  void changeTabIndex(int index) {
    tabIndex.value = index;
  }
  

  void parentIds(String? value) {
    parentId.value = value!;
    debugPrint('home parentId : $parentId');
  }

  String getFormatedPeriod(int periodValue) {
    final currentYear = periodValue.toString();
    final nextYear = (periodValue + 1).toString();
    return '$currentYear/$nextYear';
  }

  Future<void> studentData() async {
    try {
      final studentAccount = await _repoStudent.getStudentData(parentId.value);
      final studentData = Student.fromJson(studentAccount['data'][0]);
      final username = studentData.username;
      final studentIdValue = studentData.sId;
      final List<String>? studyGroupIds = studentData.studyGroupIds;
      if (studyGroupIds != null) {
        final List<StudyGroup> studyGroups = studyGroupIds
            .map((id) => findStudyGroupById(id))
            .whereType<StudyGroup>()
            .toList();

        if (studyGroups.isNotEmpty) {
          final StudyGroup firstStudyGroup = studyGroups.first;
          final int? gradeValue = firstStudyGroup.grade;
          final int? periodValue = firstStudyGroup.period;

          if (gradeValue != null && periodValue != null) {
            final String gradeRoman = convertToRomanNumeral(gradeValue);
            final String formatedPeriod = getFormatedPeriod(periodValue);

            studentId.value = studentIdValue!;
            studentName.value = username!;

            classes = [gradeRoman];
            years = [formatedPeriod];

            if (selectedClass.value.isEmpty) {
              selectedClass.value = gradeRoman;
            }
            if (selectedYear.value.isEmpty) {
              selectedYear.value = formatedPeriod;
            }
            debugPrint('Student Id : $studentId');
            debugPrint('Username : $studentName');
            debugPrint('Grade : $selectedClass');
            debugPrint('period : $selectedYear');
          }
        } else {
          debugPrint('No study group found for the student');
        }
      }
    } catch (e) {
      debugPrint('Error retrieving account data: $e');
    }
    // Using Get.find to get the instance of LoginController dynamically when needed.
    final loginController = Get.find<LoginController>();
    loginController.parentIds(parentId.value);
    loginController.studentData();
  }

  StudyGroup? findStudyGroupById(String id) {
    return null;
  }

  // Helper function to convert an integer to a Roman numeral
  String convertToRomanNumeral(int number) {
    final List<String> romanNumerals = ['XII', 'XI', 'X', 'IX', 'VIII', 'VII'];
    final List<int> decimalValues = [12, 11, 10, 9, 8, 7];

    String romanNumeral = '';
    int i = 0;

    while (number > 0) {
      if (number >= decimalValues[i]) {
        romanNumeral += romanNumerals[i];
        number -= decimalValues[i];
      } else {
        i++;
      }
    }

    return romanNumeral;
  }
}
