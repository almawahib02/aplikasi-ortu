import 'package:aplikasi_ortu/app/extentions/color.dart';
import 'package:flutter/material.dart';

class AppColors {
  static Color edbxPrimary = HexColor("024102");
  static Color edbxWhite = HexColor("FFFFFF");
  static Color edbxBlack = HexColor("000000");
  static Color edbxGreen = HexColor("FF4CAF50");
  static Color edbxGreenLight = HexColor("FF9DDB8A");
  static Color edbxGrey1 = HexColor("888888");
}