import 'package:flutter/material.dart';

class Constants {
  static const loginLogo = "KP";
  static const login = "Login";
  static const phoneHintText = "(+62) 89";
  static const phoneLabelText = "Phone number";
  static const prefixPhoneText = "(+62)";
  static const validatorPhoneTextFalse = "Enter valid number";
  static const resendingText = "Kirim ulang OTP";
  static const resendingTextBtn = "disini";
  static const sendingText = "Send OTP";
  static const otpText = "Masukan kode OTP yang kamu terima";
  static const salam = "Assalamualaikum, Orang tua/Wali";
  static const kelasText = "Kelas";
  static const periodText = "Tahun ajaran";

  static const List<String> itemMenuDesc = [
    'Biaya Sekolah',
    'Kehadiran',
    'Progress',
    'Nilai Ujian',
    'eRapor',
    'Tugas',
    'Jadwal Pelajaran',
    'Lembar Komunikasi',
    'Pengumuman'
  ];

  static const List<IconData> iconMenu = [
    Icons.account_balance_wallet,
    Icons.alarm,
    Icons.bar_chart,
    Icons.book,
    Icons.school,
    Icons.task,
    Icons.calendar_month,
    Icons.chat_bubble,
    Icons.volume_up
  ];
}
