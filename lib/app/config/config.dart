import 'package:flutter/foundation.dart';


@immutable
class Config {
  const Config._();

  static const baseUrl = String.fromEnvironment(
    'BASE_URL',
    defaultValue: 'https://edbx.pinisi.io/',
  );

  static const bearerToken = String.fromEnvironment(
    'BEARER_TOKEN',
    defaultValue: 'eyJhbGciOiJIUzI1NiIsInR5cCI6ImFjY2VzcyJ9.eyJpYXQiOjE2ODgzNTQ1MDEsImV4cCI6MTY5MDk0NjUwMSwiYXVkIjoiaHR0cHM6Ly9lZGJ4LnBpbmlzaS5pbyIsImlzcyI6ImV4cHJlc3MiLCJzdWIiOiI2MTNmOTQ4MjczN2JhZjdhNDM4MjEyNTciLCJqdGkiOiIxMDI2ZmNhYy0zNjBjLTQyOGMtOTA2ZC03OTZmMDUwYmUxNGMifQ.81pXqOaRMMMWXRjA2P0XFXFFHcokal38eSBNP_MDS8U',
  );
}