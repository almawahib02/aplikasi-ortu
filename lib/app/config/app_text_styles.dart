import 'package:flutter/material.dart';
import 'app_colors.dart';

class AppTextStyle {
  static TextStyle loginLogo = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: AppColors.edbxBlack,
  );

  static TextStyle login = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: AppColors.edbxWhite,
  );

  static TextStyle appbarText =
      TextStyle(color: AppColors.edbxWhite, fontSize: 10.5);

  static TextStyle itemMenuDesc = TextStyle(
      color: AppColors.edbxPrimary, fontSize: 11, fontWeight: FontWeight.bold);

  static TextStyle appbarDrop = TextStyle(
      color: AppColors.edbxPrimary, fontSize: 12, fontWeight: FontWeight.bold);
}
