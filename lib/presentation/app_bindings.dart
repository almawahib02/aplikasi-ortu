import 'package:aplikasi_ortu/domain/repositories/login_repository.dart';
import 'package:aplikasi_ortu/domain/repositories/user_repository.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../app/modules/home/controllers/home_controller.dart';
import '../app/modules/login/controllers/login_controller.dart';
import '../app_preferences.dart';
import '../data/network/api_service.dart';
import '../domain/repositories/student_repository.dart';
import '../domain/repositories/student_repository_impl.dart';
import '../domain/repositories/user_repository_impl.dart';
import '../domain/repositories/login_repository_impl.dart';
import '../domain/usecase/student_usecase.dart';
import '../domain/usecase/user_usecase.dart';
import '../domain/usecase/login_usecase.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    // Create and set up the Dio instance
    Dio dio = Dio();

    // ApiService is a singleton, so it will be created once and reused throughout the app
    Get.lazyPut<ApiService>(() => ApiService(dio));

    // RepositoryImpl implements Repository, so we bind UserRepositoryImpl to UserRepository
    Get.lazyPut<UserRepository>(
        () => UserRepositoryImpl(Get.find<ApiService>()));
    Get.lazyPut<StudentRepository>(
        () => StudentRepositoryImpl(Get.find<ApiService>()));
    Get.lazyPut<LoginRepository>(
        () => LoginRepositoryImpl(Get.find<ApiService>()));

    // UseCase depends on Repository, inject UserRepository into UserUseCase
    Get.lazyPut<UserUseCase>(() => UserUseCase(Get.find<UserRepository>()));
    Get.lazyPut<StudentUseCase>(
        () => StudentUseCase(Get.find<StudentRepository>()));
    Get.lazyPut<LoginUseCase>(() => LoginUseCase(Get.find<LoginRepository>()));

    // AppPreferences is a singleton, so it will be created once and reused throughout the app
    Get.put(AppPreferences());

    // Create the Controllers with the Repository instances
    Get.lazyPut<LoginController>(() => LoginController(
      Get.find<UserRepository>(),
      Get.find<LoginRepository>(),
    ));

    Get.lazyPut<HomeController>(() => HomeController(Get.find<StudentRepository>()));
  }
}
