import 'package:aplikasi_ortu/app/config/config.dart';
import 'package:dio/dio.dart';

class ApiService {
  final Dio _dio;

  ApiService(this._dio) {
    _dio.options.baseUrl = Config.baseUrl;
    _dio.options.headers['Authorization'] = 'Bearer ${Config.bearerToken}';
    _dio.options.headers['Content-type'] = 'application/json';
  }

  Future<Response<T>> get<T>(String endpoint) async {
    try {
      final response = await _dio.get<T>(endpoint);
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response<T>> post<T>(String endpoint, {dynamic data}) async {
    try {
      final response = await _dio.post<T>(endpoint, data: data);
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
