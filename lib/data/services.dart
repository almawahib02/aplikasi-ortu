import '../app/modules/home/controllers/home_controller.dart';
import '../domain/repositories/login_repository.dart';
import '../domain/repositories/user_repository.dart';

class AuthService {
  final UserRepository _userRepo;
  final LoginRepository _loginRepo;
  final HomeController _homeController;

  AuthService(this._userRepo, this._loginRepo, this._homeController);

  // Add your shared functionality methods here
  // For example, the getOtp(), createAccountAPI(), verifyOTP(), etc.
}
