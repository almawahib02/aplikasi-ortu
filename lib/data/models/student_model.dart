class Student {
  String? parentId;
  String? username;
  String? email;
  String? password;
  String? phone;
  List<int>? role;
  String? studentAtInstitutionIds;
  String? studyGroupIds;

  Student(
      {this.parentId,
      this.username,
      this.email,
      this.password,
      this.phone,
      this.role,
      this.studentAtInstitutionIds,
      this.studyGroupIds});

  Student.fromJson(Map<String, dynamic> json) {
    parentId = json['parentId'];
    username = json['username'];
    email = json['email'];
    password = json['password'];
    phone = json['phone'];
    role = json['role'].cast<int>();
    studentAtInstitutionIds = json['student_at_institutionIds'];
    studyGroupIds = json['studyGroupIds'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['parentId'] = parentId;
    data['username'] = username;
    data['email'] = email;
    data['password'] = password;
    data['phone'] = phone;
    data['role'] = role;
    data['student_at_institutionIds'] = studentAtInstitutionIds;
    data['studyGroupIds'] = studyGroupIds;
    return data;
  }
}
