class User {
  String? username;
  String? email;
  String? password;
  String? phone;
  List<int>? role;
  String? institutionIds;
  String? studyGroupIds;

  User(
      {this.username,
      this.email,
      this.password,
      this.phone,
      this.role,
      this.institutionIds,
      this.studyGroupIds});

  User.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    email = json['email'];
    password = json['password'];
    phone = json['phone'];
    role = json['role'];
    institutionIds = json["institutionIds"];
    studyGroupIds = json["studyGroupIds"];
  }

  factory User.fromPhoneNumber(String phoneNumber) {
    final email = '${phoneNumber.replaceFirst('62', '')}@gmail.com';
    final username = phoneNumber.replaceFirst('+', '');

    return User(
      username: username,
      password: phoneNumber,
      email: email,
      role: [10],
      institutionIds: '60e393ddd2cea3cb3de19985',
      studyGroupIds: '6370a76128e0940702b1272c',
    );
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['username'] = username;
    data['email'] = email;
    data['password'] = password;
    data['phone'] = phone;
    data['role'] = role;
    data['institutionIds'] = institutionIds;
    data['studyGroupIds'] = studyGroupIds;
    return data;
  }
}
