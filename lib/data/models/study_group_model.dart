class StudyGroup {
  String? sId;
  List<String>? lessonIds;
  String? type;
  String? name;
  String? department;
  int? grade;
  int? period;
  String? curriculum;
  String? teacherId;
  String? institutionId;

  StudyGroup(
      {this.sId,
      this.lessonIds,
      this.type,
      this.name,
      this.department,
      this.grade,
      this.period,
      this.curriculum,
      this.teacherId,
      this.institutionId});

  StudyGroup.fromJson(Map<String, dynamic> json) {
    sId = json['_id'];
    lessonIds = json['lessonIds'].cast<String>();
    type = json['type'];
    name = json['name'];
    department = json['department'];
    grade = json['grade'];
    period = json['period'];
    curriculum = json['curriculum'];
    teacherId = json['teacherId'];
    institutionId = json['institutionId'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['_id'] = sId;
    data['lessonIds'] = lessonIds;
    data['type'] = type;
    data['name'] = name;
    data['department'] = department;
    data['grade'] = grade;
    data['period'] = period;
    data['curriculum'] = curriculum;
    data['teacherId'] = teacherId;
    data['institutionId'] = institutionId;
    return data;
  }
}
