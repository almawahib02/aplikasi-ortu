import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();

  factory AppPreferences() => _instance;

  AppPreferences._internal();

  static const String _studentIdKey = 'studentId';
  static const String _studentNameKey = 'studentName';
  static const String _selectedClassKey = 'selectedClass';
  static const String _selectedYearKey = 'selectedYear';
  static const String _parentIdKey = 'parentId';

  Future<void> saveStudentData({
    required String studentId,
    required String studentName,
    required String selectedClass,
    required String selectedYear,
    required String parentId,
  }) async {
    final preferences = await SharedPreferences.getInstance();
    preferences.setString(_studentIdKey, studentId);
    preferences.setString(_studentNameKey, studentName);
    preferences.setString(_selectedClassKey, selectedClass);
    preferences.setString(_selectedYearKey, selectedYear);
    preferences.setString(_parentIdKey, parentId);
  }

  Future<String?> getStudentId() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(_studentIdKey);
  }

  Future<String?> getStudentName() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(_studentNameKey);
  }

  Future<String?> getSelectedClass() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(_selectedClassKey);
  }

  Future<String?> getSelectedYear() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(_selectedYearKey);
  }

  Future<String?> getParentId() async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(_parentIdKey);
  }
}
