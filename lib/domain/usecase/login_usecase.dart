
import 'package:aplikasi_ortu/domain/repositories/login_repository.dart';

class LoginUseCase {
  final LoginRepository _repo;

  LoginUseCase(this._repo);

  Future<void> loginUserApi() async {
    return await _repo.loginUserApi();
  }
}