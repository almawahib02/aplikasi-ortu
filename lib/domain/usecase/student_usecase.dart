import 'package:aplikasi_ortu/domain/repositories/student_repository.dart';

class StudentUseCase {
  final StudentRepository _repo;

  StudentUseCase(this._repo);

  Future<Map<String, dynamic>> getStudentData(String parentId) async {
    return await _repo.getStudentData(parentId);
  }
}