
import '../repositories/user_repository.dart';

class UserUseCase {
  final UserRepository _userRepository;

  UserUseCase(this._userRepository);

  Future<String?> checkAccount() async {
    return await _userRepository.checkAccount();
  }
  
  Future<void> createAccount() async {
    return await _userRepository.createAccount();
  }
}