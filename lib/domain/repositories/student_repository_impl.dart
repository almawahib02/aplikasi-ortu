
import 'package:aplikasi_ortu/data/network/api_service.dart';
import 'package:aplikasi_ortu/domain/repositories/student_repository.dart';

import '../../app/config/config.dart';

class StudentRepositoryImpl implements StudentRepository {
  final ApiService _apiService;

  StudentRepositoryImpl(this._apiService);

  @override
  Future<Map<String, dynamic>> getStudentData(String parentId) async {
    try {
      final url = '${Config.baseUrl}users?parentId=$parentId&\$populate=studyGroupIds';
      final res = await _apiService.get(url);
      if (res.statusCode == 200) {
        return res.data;
      } else {
        throw Exception('Error retrieving account data: ${res.statusCode}');
      }
    } catch (e) {
      throw Exception('Error retrieving account data: $e');
    }
  }

}