abstract class StudentRepository {
  Future<Map<String, dynamic>> getStudentData(String parentId);
}