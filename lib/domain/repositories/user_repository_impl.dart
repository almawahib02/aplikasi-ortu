import 'dart:convert';
// ignore: library_prefixes
import 'package:aplikasi_ortu/domain/entities/user.dart' as modelUser;
import 'package:aplikasi_ortu/domain/repositories/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../app/config/config.dart';
import '../../data/network/api_service.dart';

class UserRepositoryImpl implements UserRepository {
  final ApiService _apiService;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Add a constructor to receive ApiService as a parameter
  UserRepositoryImpl(this._apiService);

  @override
Future<String?> checkAccount() async {
  try {
    final phoneNumber = (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
    debugPrint('checking account: $phoneNumber');
    final url = '${Config.baseUrl}users?username=$phoneNumber';

    final res = await _apiService.get<Map<String, dynamic>>(url);
    if (res.statusCode == 200) {
      final accountData = res.data;
      if (accountData != null && accountData['data'] is List && accountData['data'].isNotEmpty) {
        debugPrint('Account exists');
        final data = accountData['data'][0] as Map<String, dynamic>;
        return data.toString(); // You may need to return a specific value from the 'data' map based on your requirements
      } else {
        return 'account not found';
      }
    } else {
      return 'Failed to check account';
    }
  } catch (e) {
    return 'Error checking account existence: $e';
  }
}


  @override
  Future<void> createAccount() async {
    try {
      final user = _auth.currentUser;
      if (user != null) {
        final phoneNumber = (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
        final email = '${phoneNumber.replaceFirst('62', '')}@gmail.com';
        const url ='${Config.baseUrl}users';

        final body = modelUser.User(
          username: phoneNumber,
          email: email,
          password: '123456',
          phone: phoneNumber,
        );

        final jsonBody = jsonEncode(body.toJson());
        debugPrint('Account creation request body: $jsonBody');
        await _apiService.post(url, data: body);
      }
    } catch (e) {
      throw Exception(e);
    }
  }
  
}