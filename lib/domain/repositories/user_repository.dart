abstract class UserRepository {
  Future<String?> checkAccount();
  Future<void> createAccount();
}