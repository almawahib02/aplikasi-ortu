import 'dart:convert';

import 'package:aplikasi_ortu/app/config/config.dart';
import 'package:aplikasi_ortu/domain/entities/login.dart';
import 'package:aplikasi_ortu/domain/repositories/login_repository.dart';
import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../data/network/api_service.dart';

class LoginRepositoryImpl implements LoginRepository {
  final ApiService _apiService;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  LoginRepositoryImpl(this._apiService);
  @override
  Future<void> loginUserApi() async {
    try {
      final phoneNumber =
          (_auth.currentUser?.phoneNumber ?? '').replaceFirst('+', '');
      const url = '${Config.baseUrl}authentication';

      final body = Login(username: phoneNumber, password: '123456');

      final jsonBody = jsonEncode(body.toJson());
      debugPrint('Account Login request body: $jsonBody');

      final res = await _apiService.post(url, data: body);
      if (res.statusCode == 201) {
        debugPrint('Login Success: ${res.statusCode}');
        Get.offNamed('/home');
      } else {
        debugPrint('Login Failed: ${res.statusCode}');
      }
    } on DioError catch (e) {
      // Handle DioError
      if (e.response != null) {
        debugPrint('DioError: Status Code: ${e.response?.statusCode}');
        debugPrint('DioError: Response Data: ${e.response?.data}');
      } else {
        debugPrint('DioError: No response from the server.');
      }
      // Additional error handling or error display can be done here.
      throw Exception(
          'Login Error: An error occurred. Please try again later.');
    } catch (e) {
      throw Exception(
          'Login Error: An error occurred. Please try again later.');
    }
  }
}
